package com.example.projet_android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projet_android.Models.DbModels.Pokemon;
import com.example.projet_android.Utils.MyAdapter;
import com.example.projet_android.Utils.PokemonViewModel;

import java.util.ArrayList;

public class ModifyActivity  extends AppCompatActivity {

    private String current_id;
    private Pokemon current_poke;
    private PokemonViewModel pvm;

    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_layout);
        Intent intent = getIntent();
        String id = intent.getStringExtra("id_poke");
        current_id = id;
        pvm = new PokemonViewModel(getApplication());
        Pokemon pokemon = pvm.getPokemonById(Integer.parseInt(id));
        current_poke = pokemon;
        EditText etNom = findViewById(R.id.name_modify);
        EditText etEsp = findViewById(R.id.espece_modify);
        EditText etDesc = findViewById(R.id.description_modify);
        EditText etEvolve = findViewById(R.id.evolve_modify);

        etNom.setText(pokemon.getNom());
        etEsp.setText(pokemon.getEspece());
        etDesc.setText(pokemon.getDescription());
        if (pokemon.getId_evolvedFrom() != null){
            etEvolve.setText(pokemon.getId_evolvedFrom());
        }

    }

    public void validerModif(View view){
        EditText etNom = findViewById(R.id.name_modify);
        EditText etEsp = findViewById(R.id.espece_modify);
        EditText etDesc = findViewById(R.id.description_modify);
        EditText etEvolve = findViewById(R.id.evolve_modify);



        if (etNom.getText().toString().replace(" ","").length()==0 || etEsp.getText().toString().replace(" ","").length()==0 || etDesc.getText().toString().replace(" ","").length()==0 ){
            Toast toast = Toast.makeText(getApplicationContext(),"Les champs Nom, Espece et Description doivent être remplis.", Toast.LENGTH_SHORT);
            toast.show();

        }

        else{

            try {
                if (etEvolve.getText().toString().replace(" ", "").length()!=0) {
                    double d = Double.parseDouble(etEvolve.getText().toString());
                }


                String nom = etNom.getText().toString();
                String esp = etEsp.getText().toString();
                String desc = etDesc.getText().toString();
                String ev = etEvolve.getText().toString();

                if (!esp.split(" ")[0].equals("Pokémon")){
                    esp = "Pokémon "+esp;
                }
                if (ev.replace(" ","").length()==0){
                    ev = null;
                }

                pvm.update(Integer.parseInt(current_id), nom, esp, desc, ev, current_poke.getColor());

            } catch (NumberFormatException nfe) {
                Toast toast2 = Toast.makeText(getApplicationContext(),"Valeur invalide pour l'évolution.", Toast.LENGTH_SHORT);
                toast2.show();
            }



        }
    }


}
