package com.example.projet_android.Utils;

import com.example.projet_android.Models.Models;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PokeApiService {
    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://pokeapi.co/api/v2/pokemon-species/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    @GET("{id}")
    Call<Models> getModels(@Path("id")String id);
}
