package com.example.projet_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projet_android.Models.DbModels.Pokemon;
import com.example.projet_android.Utils.PokemonViewModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PokemonActivity   extends AppCompatActivity {

    private PokemonViewModel pvm;
    private String id_current;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pokemon_activity);
        Intent intent = getIntent();
        String id = intent.getStringExtra("id_poke");
        id_current = id;
        pvm = new PokemonViewModel(getApplication());
        Pokemon pokemon = pvm.getPokemonById(Integer.parseInt(id));
        TextView tvNom = findViewById(R.id.pokeNom);
        tvNom.setText(pokemon.getNom());
        TextView tvEspece = findViewById(R.id.pokeEspece);
        tvEspece.setText(pokemon.getEspece());
        TextView tvDescription = findViewById(R.id.pokeDescription);
        tvDescription.setText(pokemon.getDescription());
        TextView tvEvolved = findViewById(R.id.pokeEvolvedFrom);
        if (pokemon.getId_evolvedFrom() == null || Integer.parseInt(pokemon.getId_evolvedFrom()) > 151) {
            tvEvolved.setVisibility(View.INVISIBLE);
        }
        else {
            tvEvolved.setText("Evolue du pokemon n° : "+pokemon.getId_evolvedFrom());
        }
    }

    public void deletePokemon(View view){
        //gérer l'actualisation
        pvm.delete(Integer.parseInt(id_current));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            Log.d("meslogs", "onActivityResult: o");
        }
    }

    public void modifierPokemon(View view){
        Intent intent = new Intent(this, ModifyActivity.class);
        intent.putExtra("id_poke", this.id_current);
        startActivityForResult(intent, 2);
    }
}
