package com.example.projet_android.Utils;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.projet_android.Models.DbModels.Pokemon;

import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface MyDao {

    @Insert
    void insert(Pokemon pokemon);

    @Update
    void update(Pokemon pokemon);

    @Delete
    void delete(Pokemon pokemon);

    @Query("SELECT * FROM pokemon_table ORDER BY id ASC;")
    List<Pokemon> getAllPokemon();

    @Query("SELECT * FROM pokemon_table WHERE id = :id;")
    Pokemon getPokemonById(int id);

}
