package com.example.projet_android.Utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.projet_android.Models.DbModels.Pokemon;

@Database(entities = {Pokemon.class}, version = 1)
public abstract class PokemonRoomDatabase extends RoomDatabase {

    public abstract MyDao myDao();

    private static PokemonRoomDatabase INSTANCE;

    static PokemonRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (PokemonRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PokemonRoomDatabase.class, "pokemon_database").build();
                }
            }
        }
        return INSTANCE;
    }

}
