package com.example.projet_android.Utils;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.projet_android.Models.DbModels.Pokemon;

import java.util.List;

public class PokemonViewModel extends AndroidViewModel {
    private PokemonRepository pr;
    private Pokemon currentPokemon;
    private LiveData<List<Pokemon>> allPokemon;

    public PokemonViewModel(@NonNull Application application) {


        super(application);
        pr = new PokemonRepository(application);
    }

    public List<Pokemon> getAllPokemon() {

        return pr.allPokemon();
    }

    public Pokemon getPokemonById(int id) {

        return pr.getPokemonById(id);
    }

    public void setAllPokemon(LiveData<List<Pokemon>> allPokemon) {
        this.allPokemon = allPokemon;
    }

    public void insert(Integer id, String nom, String espece, String description, String id_evolvedFrom, String color){
        pr.insert(new Pokemon(id, nom, espece, description, id_evolvedFrom, color));
    }
    public void update(Integer id, String nom, String espece, String description, String id_evolvedFrom, String color){
        pr.update(new Pokemon(id, nom, espece, description, id_evolvedFrom, color));
    }

    public void delete(Integer id){

        pr.delete(pr.getPokemonById(id));
    }


}
