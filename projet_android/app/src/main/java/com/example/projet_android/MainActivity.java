package com.example.projet_android;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.projet_android.Models.DbModels.Pokemon;
import com.example.projet_android.Models.EvolvesFromSpecies;
import com.example.projet_android.Models.FlavorTextEntry;
import com.example.projet_android.Models.Genera;
import com.example.projet_android.Models.Models;
import com.example.projet_android.Models.Name;
import com.example.projet_android.Utils.MyAdapter;
import com.example.projet_android.Utils.PokeApiCalls;
import com.example.projet_android.Utils.PokemonComparator;
import com.example.projet_android.Utils.PokemonViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity  extends AppCompatActivity implements PokeApiCalls.Callbacks {

    private PokemonViewModel pvm;
    private ArrayList<Pokemon> lesPokemons = new ArrayList<>();
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Parcelable recyclerState;

    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pvm = new PokemonViewModel(getApplication());
        this.lesPokemons = (ArrayList<Pokemon>) pvm.getAllPokemon();
        if (lesPokemons.size()!=0){
            findViewById(R.id.request).setVisibility(View.INVISIBLE);
        }
        final RecyclerView recyclerView = findViewById(R.id.recyclerview_pkmn);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyAdapter(this.lesPokemons);
        recyclerView.setAdapter(mAdapter);
    }

    private void executeRequest() {
        findViewById(R.id.request).setVisibility(View.INVISIBLE);
        this.updateUIStartRequest();
        String id = "1";
        for (int i=1; i<=151;i++){
            id = Integer.toString(i);
            PokeApiCalls.fetchModels( this, id);
        }
    }

    private void updateUIStartRequest() {
    }


    @Override
    public void onResponse(@Nullable Models models) {
        if(models!=null){
            this.updateUIModels(models);
        }
    }

    private void updateUIModels(Models models) {
        List<Name> names = models.getNames();
        List<FlavorTextEntry> textEntries = models.getFlavorTextEntries();
        List<Genera> generaList = models.getGenera();
        EvolvesFromSpecies evolvesFromSpecies = models.getEvolvesFromSpecies();
        String s_name = "";
        String s_descrip= "";
        String s_genus = "";
        String s_color = models.getColor().getName();
        String url_evolved = "";
        String id_evolved = null;

        if (evolvesFromSpecies!=null){
            url_evolved = evolvesFromSpecies.getUrl();
            id_evolved = url_evolved.split("/")[url_evolved.split("/").length-1];

        }


        for (Name name : names)
        {

            if (name.getLanguage().getName().equals("fr"))  s_name = name.getName();
        }

        for (FlavorTextEntry flavorTextEntry:  textEntries){
            if (flavorTextEntry.getLanguage().getName().equals("fr"))  s_descrip = flavorTextEntry.getFlavorText();
        }
        for (Genera genera : generaList)
        {

            if (genera.getLanguage().getName().equals("fr"))  s_genus = genera.getGenus();
        }


        Log.d("mes logs", s_descrip);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(s_name+"\n"+s_genus+"\n"+s_descrip+"\n Evolu depuis le pkmn n°: "+id_evolved);
        pvm.insert(models.getId(),s_name,s_genus,s_descrip,id_evolved, s_color);
        this.lesPokemons.add(new Pokemon(models.getId(),s_name,s_genus,s_descrip,id_evolved, s_color));

        Collections.sort(this.lesPokemons, new PokemonComparator());
        updateUIStop(stringBuilder.toString());
        mAdapter.notifyDataSetChanged();
    }

    private void updateUIStop(String s) {
    }





    @Override
    public void onFailure() {

    }

    public void requeteApi(View view){
        this.executeRequest();
    }

    public void appelBd(View view){
        List<Pokemon> list_pokemon = this.pvm.getAllPokemon();
        TextView textView = findViewById(R.id.allPokemon);
        textView.setText(list_pokemon.toString());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            this.lesPokemons = (ArrayList<Pokemon>) pvm.getAllPokemon();
            mAdapter.notifyDataSetChanged();
        }
    }

    public void ajouterPokemon(View view){
        Intent intent = new Intent(this, AjouterActivity.class);
        startActivityForResult(intent, 1);
    }
}
