package com.example.projet_android.Models.DbModels;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "pokemon_table")
public class Pokemon {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name="id")
    public Integer id;

    @NonNull
    @ColumnInfo(name="name")
    public String nom;

    @NonNull
    @ColumnInfo(name="specie")
    public String espece;

    @NonNull
    @ColumnInfo(name="description")
    public String description;

    @ColumnInfo(name="evolvedFrom")
    public String id_evolvedFrom;

    @NonNull
    @ColumnInfo(name="color")
    public String color;

    public Pokemon(Integer id, String nom, String espece, String description, String id_evolvedFrom, String color) {
        this.id = id;
        this.nom = nom;
        this.espece = espece;
        this.description = description;
        this.id_evolvedFrom = id_evolvedFrom;
        this.color = color;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public String getEspece() {
        return espece;
    }

    public void setEspece(@NonNull String espece) {
        this.espece = espece;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getId_evolvedFrom() {
        return id_evolvedFrom;
    }

    public void setId_evolvedFrom(@NonNull String id_evolvedFrom) {
        this.id_evolvedFrom = id_evolvedFrom;
    }

    @NonNull
    public String getColor() {
        return color;
    }

    public void setColor(@NonNull String color) {
        this.color = color;
    }



    public String toString(){
        return this.nom;
    }
}
