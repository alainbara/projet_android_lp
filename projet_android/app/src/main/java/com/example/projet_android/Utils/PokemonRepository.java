package com.example.projet_android.Utils;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.example.projet_android.Models.DbModels.Pokemon;

import java.util.List;

public class PokemonRepository {

    private MyDao myDao;

    public PokemonRepository(Application application){
        PokemonRoomDatabase db = PokemonRoomDatabase.getDatabase(application);
        myDao = db.myDao();
    }

    public void insert(Pokemon pokemon){
        new insertAsyncTask(myDao).execute(pokemon);
    }

    private static class insertAsyncTask extends AsyncTask<Pokemon,Void,Void> {
        private MyDao asyncDao;

        insertAsyncTask(MyDao myDao){asyncDao=myDao;}

        @Override
        protected Void doInBackground(Pokemon... pokemons) {
            asyncDao.insert(pokemons[0]);
            return null;
        }
    }

    public void update(Pokemon pokemon){
        new UpdateAsyncTask(myDao).execute(pokemon);
    }

    private static class UpdateAsyncTask extends AsyncTask<Pokemon,Void,Void>{
        private MyDao asyncDao;
        public UpdateAsyncTask(MyDao myDao) {
            asyncDao=myDao;
        }

        @Override
        protected Void doInBackground(Pokemon... pokemons) {
            asyncDao.update(pokemons[0]);
            return null;
        }
    }

    public void delete(Pokemon pokemon){
        new deleteAsyncTask(myDao).execute(pokemon);
    }

    private static class deleteAsyncTask extends AsyncTask<Pokemon,Void,Void> {
        private MyDao asyncDao;

        deleteAsyncTask(MyDao myDao){asyncDao=myDao;}

        @Override
        protected Void doInBackground(Pokemon... pokemons) {
            asyncDao.delete(pokemons[0]);
            return null;
        }

    }

    public List<Pokemon> allPokemon() {
        try {

            return (List<Pokemon>) new allPokemonAsyncTask(myDao).execute().get();
        }catch (Exception e){
            Log.d("Mes logs","pb getAll");
            return null;
        }
    }


    private class allPokemonAsyncTask extends AsyncTask<Void,Void,List<Pokemon>>{
        private MyDao asyncDao;

        allPokemonAsyncTask(MyDao myDao){asyncDao=myDao;}

        @Override
        protected List<Pokemon> doInBackground(Void... voids) {
            return asyncDao.getAllPokemon();
        }
    }

    public Pokemon getPokemonById(int id) {
        try {
            //erreur ici
            return new getPokemonByIdAsyncTask(myDao).execute(id).get();
        }catch (Exception e){
            Log.d("Mes logs","pb getPkmById");
            return null;
        }
    }

    private class getPokemonByIdAsyncTask extends AsyncTask<Integer,Void,Pokemon>{
        private MyDao asyncDao;

        getPokemonByIdAsyncTask(MyDao myDao){asyncDao=myDao;}


        @Override
        protected Pokemon doInBackground(Integer... integers) {
            return asyncDao.getPokemonById(integers[0]);
        }
    }


}
