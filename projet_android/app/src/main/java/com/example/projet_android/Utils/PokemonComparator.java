package com.example.projet_android.Utils;

import com.example.projet_android.Models.DbModels.Pokemon;

import java.util.Comparator;

public class PokemonComparator implements Comparator<Pokemon>
{
    // Used for sorting in ascending order of
    // roll number


    @Override
    public int compare(Pokemon pokemon, Pokemon t1) {
        return pokemon.getId() - t1.getId();
    }
}