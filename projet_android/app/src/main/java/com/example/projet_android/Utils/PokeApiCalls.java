package com.example.projet_android.Utils;

import androidx.annotation.Nullable;

import com.example.projet_android.Models.Models;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokeApiCalls {
    public interface Callbacks{
        void onResponse(@Nullable Models models);
        void onFailure();
    }

    public static void fetchModels(final Callbacks callbacks, String id){
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<Callbacks>(callbacks);
        PokeApiService pokeApiService = PokeApiService.retrofit.create(PokeApiService.class);
        Call<Models> call = pokeApiService.getModels(id);
        call.enqueue(new Callback<Models>() {
            @Override
            public void onResponse(Call<Models> call, Response<Models> response) {
                if (callbacksWeakReference.get()!=null){
                    callbacksWeakReference.get().onResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<Models> call, Throwable t) {

            }
        });
    }
}
