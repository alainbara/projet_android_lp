package com.example.projet_android.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projet_android.Models.DbModels.Pokemon;
import com.example.projet_android.PokemonActivity;
import com.example.projet_android.R;

import java.util.ArrayList;

public class MyAdapter  extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private static ArrayList<Pokemon> lesPokemons;

    public MyAdapter(ArrayList<Pokemon>  lesPokemons) {
        this.lesPokemons = lesPokemons;
    }

    public void setLesPokemons(ArrayList<Pokemon> lesPokemons){
        this.lesPokemons = lesPokemons;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cells, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return  myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Pokemon p = lesPokemons.get(position);

        holder.display(p);
    }

    @Override
    public int getItemCount() {
        return (lesPokemons == null ) ? 0 : this.lesPokemons.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvPoke;
        private Pokemon current;
        public MyViewHolder(View view) {
            super(view);
            this.tvPoke = view.findViewById(R.id.tvPoke);
            view.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Activity context = (Activity)view.getContext();
                    Log.d("MesLogs", "click");
                    String id = Integer.toString(current.getId());
                    Log.d("meslogs", id);
                    Intent intent = new Intent(context, PokemonActivity.class);
                    intent.putExtra("id_poke", id);
                    context.startActivityForResult(intent, 1);
                }
            });
        }

        public void display(Pokemon p){
            current = p;
            tvPoke.setText("n°"+p.getId()+" "+p.getNom());
            if (p.getColor().equals("yellow")){
                tvPoke.setBackgroundColor(Color.YELLOW);
            }
            if (p.getColor().equals("green")){
                tvPoke.setBackgroundColor(Color.GREEN);
            }
            if (p.getColor().equals("red")){
                tvPoke.setBackgroundColor(Color.RED);
            }
            if (p.getColor().equals("blue")){
                tvPoke.setBackgroundColor(Color.BLUE);
            }

            if (p.getColor().equals("white")){
                tvPoke.setBackgroundColor(Color.WHITE);
            }
            if (p.getColor().equals("pink")){
                tvPoke.setBackgroundColor(Color.MAGENTA);
            }
            if (p.getColor().equals("gray")){
                tvPoke.setBackgroundColor(Color.GRAY);
            }
            if (p.getColor().equals("brown")){
                tvPoke.setBackgroundColor(Color.parseColor("#DEB887"));
            }
            if (p.getColor().equals("purple")){
                tvPoke.setBackgroundColor(Color.parseColor("#9370DB"));
            }
        }

    }
}
