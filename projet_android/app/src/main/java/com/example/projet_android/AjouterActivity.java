package com.example.projet_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projet_android.Models.DbModels.Pokemon;
import com.example.projet_android.Utils.PokemonViewModel;

import java.util.ArrayList;
import java.util.List;

public class AjouterActivity extends AppCompatActivity {

    private PokemonViewModel pvm;
    private Integer next_id = 1;
    protected void onCreate(Bundle savedInstanceState) {


        pvm = new PokemonViewModel(getApplication());
        ArrayList<Pokemon> allPokemon = (ArrayList<Pokemon>) pvm.getAllPokemon();
        if (allPokemon.size()!=0){

        next_id = allPokemon.get(allPokemon.size()-1).getId()+1;
        }



        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_layout);




    }

    public void addValidate(View view){
        EditText etNom = findViewById(R.id.ajout_nom);
        EditText etEsp = findViewById(R.id.ajout_espece);
        EditText etDesc = findViewById(R.id.ajout_description);
        EditText etEvolve = findViewById(R.id.ajout_evolve);

        if (etNom.getText().toString().replace(" ","").length()==0 || etEsp.getText().toString().replace(" ","").length()==0 || etDesc.getText().toString().replace(" ","").length()==0 ){
            Toast toast = Toast.makeText(getApplicationContext(),"Les champs Nom, Espece et Description doivent être remplis.", Toast.LENGTH_SHORT);
            toast.show();

        }

        else{

            try {
                if (etEvolve.getText().toString().replace(" ", "").length()!=0) {
                    double d = Double.parseDouble(etEvolve.getText().toString());
                }


                String nom = etNom.getText().toString();
                String esp = etEsp.getText().toString();
                String desc = etDesc.getText().toString();
                String ev = etEvolve.getText().toString();

                if (!esp.split(" ")[0].equals("Pokémon")){
                    esp = "Pokémon "+esp;
                }
                if (ev.replace(" ","").length()==0){
                    ev = null;
                }

                pvm.insert(next_id, nom, esp, desc, ev, "green");

            } catch (NumberFormatException nfe) {
                Toast toast2 = Toast.makeText(getApplicationContext(),"Valeur invalide pour l'évolution.", Toast.LENGTH_SHORT);
                toast2.show();
            }



        }
    }
}
